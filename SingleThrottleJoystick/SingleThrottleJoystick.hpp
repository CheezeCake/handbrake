#ifndef _SINGLE_THROTTLE_JOYSTICK_HPP_
#define _SINGLE_THROTTLE_JOYSTICK_HPP_

#include <HID.h>

#if !defined(_USING_HID)
# error "Using legacy HID core (non pluggable)"
#endif

class SingleThrottleJoystick
{
	public:
		SingleThrottleJoystick();

		void setValue(uint8_t value);
		void sendReport();

	private:
		uint8_t value;
};

#endif
