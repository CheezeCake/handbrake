#include "SingleThrottleJoystick.hpp"

static const uint8_t _hidReportDescriptor[] PROGMEM = {
	0x05, 0x01,       // UsagePage (Generic Desktop)
	0x09, 0x04,       // Usage (Joystick)
	0xa1, 0x01,       // Collection (Application)
	0x85, 0x01,       // 	ReportID (1)
	0x05, 0x02,       // 	UsagePage (Simulation Controls)
	0x09, 0xbb,       // 	Usage (Throttle)
	0x15, 0x00,       // 	Logical Minimum(0)
	0x26, 0xff, 0x00, // 	Logical Maximum(255)
	0x75, 0x08,       // 	Report Size(8)
	0x95, 0x01,       // 	Report Count(1)
	0x81, 0x02,       // 	Input (Data, Variable, Absolute)
	0xc0,             // End Collection ()
};

SingleThrottleJoystick::SingleThrottleJoystick() : value(0)
{
	static HIDSubDescriptor node(_hidReportDescriptor, sizeof(_hidReportDescriptor));
	HID().AppendDescriptor(&node);
}

void SingleThrottleJoystick::setValue(uint8_t value)
{
	this->value = value;
	sendReport();
}

void SingleThrottleJoystick::sendReport()
{
	HID().SendReport(1, &value, 1);
}
